const crypto = require('crypto');

function uid() {
  return `${crypto.randomBytes(5).toString('hex')}-${Date.now()}`;
}

module.exports = uid;
